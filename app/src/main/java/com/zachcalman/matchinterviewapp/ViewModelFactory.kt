package com.zachcalman.matchinterviewapp

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zachcalman.matchinterviewapp.data.repositories.NewsRepository
import com.zachcalman.matchinterviewapp.ui.main.ArticleListViewModel
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class ViewModelFactory private constructor(
    private val newsRepository: NewsRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass) {
            when {
                isAssignableFrom(ArticleListViewModel::class.java) ->
                    ArticleListViewModel(newsRepository)
                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: $modelClass.name")
            }
        } as T

    companion object {
        private var INSTANCE: ViewModelFactory? = null
        private val lock = Any()
        fun getInstance(application: Application): ViewModelFactory {
            INSTANCE ?: synchronized(lock) {
                INSTANCE?: ViewModelFactory(
                    Injection.provideNewsRepository(application.applicationContext)
                )
            }.also { INSTANCE = it }

            return INSTANCE!!
        }
    }
}