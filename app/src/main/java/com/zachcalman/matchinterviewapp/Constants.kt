package com.zachcalman.matchinterviewapp

import retrofit2.Retrofit

class Constants {

    companion object {
        const val NEWS_API_URL = "https://newsapi.org/v2/"
        const val NEWS_API_KEY = BuildConfig.NEWS_API_KEY
    }
}