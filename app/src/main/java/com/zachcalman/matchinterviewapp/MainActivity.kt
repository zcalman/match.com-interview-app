package com.zachcalman.matchinterviewapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.zachcalman.matchinterviewapp.ui.main.ArticleListViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        //Set up action bar to work with the nav controller
        val navController = findNavController(R.id.nav_host_fragment)
        val appbarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appbarConfiguration)

//        toolbar {
//            this.setNavigationOnClickListener {
//                navController.navigateUp(appbarConfiguration) || super.onSupportNavigateUp()
//            }
//        }
    }

    fun obtainArticleListViewModel(): ArticleListViewModel =
        obtainViewModel(ArticleListViewModel::class.java)

    override fun onSupportNavigateUp(): Boolean {
        // Make the back arrow in the action bar work
        val navController = findNavController(R.id.nav_host_fragment)
        val appbarConfiguration = AppBarConfiguration(navController.graph)
        return navController.navigateUp(appbarConfiguration) || super.onSupportNavigateUp()
    }
}

private fun<T : ViewModel> AppCompatActivity.obtainViewModel(viewModelClass: Class<T>) =
    ViewModelProviders.of(this, ViewModelFactory.getInstance(application)).get(viewModelClass)