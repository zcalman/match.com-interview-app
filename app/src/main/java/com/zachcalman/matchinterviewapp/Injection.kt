package com.zachcalman.matchinterviewapp

import android.content.Context
import com.zachcalman.matchinterviewapp.data.local.AppDatabase
import com.zachcalman.matchinterviewapp.data.remote.ApiFactory
import com.zachcalman.matchinterviewapp.data.repositories.NewsRepository

object Injection {
    fun provideNewsRepository(context: Context): NewsRepository {
        return NewsRepository.getInstance(AppDatabase.getInstance(context).articleDao(), ApiFactory.newsApiService)
    }
}