package com.zachcalman.matchinterviewapp.data.remote

import android.util.Log
import com.zachcalman.matchinterviewapp.data.Article
import com.zachcalman.matchinterviewapp.data.NewsApiModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Container for all search functions related to the remote source (API)
 */
object SearchFunctions {

    private const val TAG = "News.SearchFunctions"

    /**
     * Query all top headlines based on a specific query string and country
     *
     */
    fun searchTopHeadlines(
        service: NewsApiService,
        query: String?,
        country: String,
        page: Int,
        pageSize: Int,
        onSuccess: (articles: List<Article>, totalResults: Int) -> Unit,
        onError: (error: String) -> Unit
    ) {
        Log.i(TAG, "Search top headlines, query: ${query ?: "_"}, country: $country, page: $page, itemsPerPage: $pageSize")

        service.topHeadlines(country, null, null, query, pageSize, page).enqueue(
            object : Callback<NewsApiModel> {
                override fun onFailure(call: Call<NewsApiModel>, t: Throwable) {
                    Log.e(TAG, "Failed to get data")
                    onError(t.message ?: "unknown error")
                }

                override fun onResponse(
                    call: Call<NewsApiModel>,
                    response: Response<NewsApiModel>
                ) {
                    Log.i(TAG, "Retrieved response from NewsAPI")
                    if (response.isSuccessful) {
                        if (response.body()?.status != "error") {
                            val articles = response.body()?.articles ?: emptyList()
                            val totalResults = response.body()?.totalResults?: -1
                            onSuccess(articles, totalResults)
                        }

                    } else {
                        onError(response.errorBody()?.string() ?: "unknown error")
                    }
                }

            }
        )
    }
}