package com.zachcalman.matchinterviewapp.data

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Model to represent API response data
 */

/**
 * Used for both /top-headlines and /everything API requests
 * @param status If the request was successful or not. Options: ok, error. In the case of error a code and message property will be populated.
 * @param totalResults The total number of results available for your request.
 * @param articles The results of the request.
 */
data class NewsApiModel (
    @SerializedName("status") val status: String,
    @SerializedName("totalResults") val totalResults: Int,
    @SerializedName("articles") val articles: List<Article>
)

/**
 * /top-headlines and /everything returns a list of many Articles, each having these properties
 * @param source The identifier id and a display name name for the source this article came from.
 * @param author The author of the article
 * @param title The headline or title of the article.
 * @param description A description or snippet from the article.
 * @param articleUrl The direct URL to the article.
 * @param imageUrl The URL to a relevant image for the article.
 * @param publishedAt The date and time that the article was published, in UTC (+000)
 * @param content The unformatted content of the article, where available. This is truncated to 260 chars for Developer plan users.
 */
@Entity(tableName = "articles")
data class Article (
    @PrimaryKey(autoGenerate = true) val uid: Int,
    @Embedded @SerializedName("source") val source: Source?,
    @ColumnInfo(name = "author") @SerializedName("author") val author: String?,
    @ColumnInfo(name = "title") @SerializedName("title") val title: String?,
    @ColumnInfo(name = "description") @SerializedName("description") val description: String?,
    @ColumnInfo(name = "article_url") @SerializedName("url") val articleUrl: String?,
    @ColumnInfo(name = "image_url") @SerializedName("urlToImage") val imageUrl: String?,
    @ColumnInfo(name = "published_at") @SerializedName("publishedAt") val publishedAt: String?,
    @ColumnInfo(name = "content") @SerializedName("content") val content: String?,
    @ColumnInfo(name = "type") var type: String,
    @ColumnInfo(name = "expanded") var expanded: Boolean
)

/**
 * The source of a specific article
 * @param id Identifier of a source, usually lower case and hyphenated
 * @param name Full name of the source
 */
data class Source (
    @ColumnInfo(name = "source_id") val id: String?,
    @ColumnInfo(name = "source_name") val name: String?
)

/**
 * /v2/sources
 * This endpoint returns the subset of news publishers that top headlines (/v2/top-headlines) are
 * available from.
 * @param status If the request was successful or not. Options: ok, error. In the case of error a code and message property will be populated.
 * @param sources The results of the request.
 */
data class SourceApiModel (
    val status: String,
    val sources: List<SourceExtra>
)

/**
 * Full data about each source
 * @param id The identifier of the news source
 * @param name The name of the news source
 * @param description A description of the news source
 * @param url The URL of the homepage.
 * @param category The type of news to expect from this news source.
 * @param language The language that this news source writes in.
 * @param country The country this news source is based in (and primarily writes about).
 */
data class SourceExtra (
    val id: String,
    val name: String,
    val description: String,
    val url: String,
    val category: String,
    val language: String,
    val country: String
)