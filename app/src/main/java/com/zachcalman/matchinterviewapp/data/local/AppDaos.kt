package com.zachcalman.matchinterviewapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.paging.DataSource
import androidx.room.Delete
import com.zachcalman.matchinterviewapp.data.Article

@Dao
interface ArticleDao {
    @Query("SELECT * FROM articles")
    fun getAllArticles(): DataSource.Factory<Int, Article>

    @Query("SELECT * FROM articles WHERE type = :type")
    fun getArticles(type: String): DataSource.Factory<Int, Article>

    @Insert
    fun insertArticles(articles: List<Article>)

    @Query("DELETE FROM articles WHERE type = :type")
    fun deleteArticlesByType(type: String)
}
