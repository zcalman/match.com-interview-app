package com.zachcalman.matchinterviewapp.data.remote

import com.zachcalman.matchinterviewapp.data.NewsApiModel
import com.zachcalman.matchinterviewapp.data.SourceApiModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Defines the actual API calls and their query parameters
 */
interface NewsApiService {

    /**
     * Retrieves top headlines based on query parameters, with the added ability to specify page size and what page you want to query for
     * @param country The 2-letter ISO 3166-1 code of the country you want to get headlines for. Note: you can't mix this param with the sources param.
     * @param category The category you want to get headlines for. Possible options: business entertainment general health science sports technology . Note: you can't mix this param with the sources param.
     * @param sources A comma-separated string of identifiers for the news sources or blogs you want headlines from. Note: you can't mix this param with the country or category params.
     * @param q Keywords or a phrase to search for
     * @param pageSize The number of results to return per page (request). 20 is the default, 100 is the maximum.
     * @param page Use this to page through the results if the total results found is greater than the page size.
     */
    @GET("top-headlines")
    fun topHeadlines (
        @Query("country") country: String,
        @Query("category") category: String?,
        @Query("sources") sources: List<String>?,
        @Query("q") q: String?,
        @Query("pageSize") pageSize: Int?,
        @Query("page") page: Int?
    ): Call<NewsApiModel>

    /**
     * Search through all articles with specified query parameters, with the added ability to specify page size and what page you want to query for
     * @param q Keywords or phrases to search for in the article title and body. Supports advanced search.
     * @param qInTitle Keywords or phrases to search for in the article title only.
     * @param sources A comma-separated string of identifiers (maximum 20) for the news sources or blogs you want headlines from
     * @param domains A comma-separated string of domains (eg bbc.co.uk, techcrunch.com, engadget.com) to restrict the search to.
     * @param excludeDomains A comma-seperated string of domains (eg bbc.co.uk, techcrunch.com, engadget.com) to remove from the results.
     * @param from A date and optional time for the oldest article allowed. This should be in ISO 8601 format (e.g. 2019-11-12 or 2019-11-12T05:19:38) Default: the oldest according to your plan.
     * @param to A date and optional time for the newest article allowed. This should be in ISO 8601 format (e.g. 2019-11-12 or 2019-11-12T05:19:38) Default: the newest according to your plan.
     * @param language The 2-letter ISO-639-1 code of the language you want to get headlines for. Possible options: ardeenesfrheitnlnoptruseudzh. Default: all languages returned.
     * @param sortBy The order to sort the articles in.
     * @param pageSize The number of results to return per page (request). 20 is the default, 100 is the maximum.
     * @param page Use this to page through the results if the total results found is greater than the page size.
     */
    @GET("everything")
    fun everything (
        @Query("q") q: String?,
        @Query("qIntTitle") qInTitle: String?,
        @Query("sources") sources: List<String>?,
        @Query("domains") domains: List<String>?,
        @Query("excludeDomains") excludeDomains: List<String>?,
        @Query("from") from: String?,
        @Query("to") to: String?,
        @Query("language") language: String?,
        @Query("sortBy") sortBy: String?,
        @Query("pageSize") pageSize: Int?,
        @Query("page") page: Int?
    ) : Call<NewsApiModel>

    /**
     * Queries the subset of publishers that post top headlines
     * @param category Find sources that display news of this category
     * @param language Find sources that display news in a specific language.
     * @param country Find sources that display news in a specific country.
     */
    @GET("sources")
    fun sources (
        @Query("category") category: String?,
        @Query("language") language: String?,
        @Query("country") country: String?
    ) : Call<SourceApiModel>
}