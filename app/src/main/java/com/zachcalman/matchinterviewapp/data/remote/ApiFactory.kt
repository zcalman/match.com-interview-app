package com.zachcalman.matchinterviewapp.data.remote

import com.zachcalman.matchinterviewapp.Constants
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Builds the API calls and inserts the API key
 */
object ApiFactory {
    private fun getAuthInterceptor(key: String) = Interceptor { chain ->
        val newUrl = chain.request().url()
            .newBuilder()
            .build()

        val newRequest = chain.request()
            .newBuilder()
            .url(newUrl)
            .header("x-api-key", key)
            .build()

        chain.proceed(newRequest)
    }

    private fun getClient(key: String) = OkHttpClient().newBuilder()
        .addInterceptor(getAuthInterceptor(key))
        .build()

    private fun retrofit(url: String, key: String) : Retrofit = Retrofit.Builder()
        .client(getClient(key))
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val newsApiService : NewsApiService = retrofit(Constants.NEWS_API_URL, Constants.NEWS_API_KEY).create(NewsApiService::class.java)
}