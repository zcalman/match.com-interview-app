package com.zachcalman.matchinterviewapp.data.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import com.zachcalman.matchinterviewapp.data.ArticleSearchResult
import com.zachcalman.matchinterviewapp.data.local.ArticleDao
import com.zachcalman.matchinterviewapp.data.paging.NewsListBoundaryCallback
import com.zachcalman.matchinterviewapp.data.remote.NewsApiService
import com.zachcalman.matchinterviewapp.data.remote.SearchFunctions.searchTopHeadlines
import com.zachcalman.matchinterviewapp.ui.main.Query
import org.jetbrains.anko.doAsync

/**
 * Central repository to request data from the API, as well as store data locally
 */
class NewsRepository(private val articleDao: ArticleDao, private val newsApiService: NewsApiService) {
    private var isRequestInProgress: Boolean = false
    private var lastRequestedPage: Int = 1

    private val networkErrors = MutableLiveData<String>()

    private var topHeadlinesTotalResults: Int = -1

    private lateinit var cachedQuery: Query

    /**
     * Responsible for first querying and then caching all top headlines articles as they are requested
     */
    private fun queryAndInsertTopHeadlines(query: Query) {
        if (isRequestInProgress) return

        isRequestInProgress = true
        searchTopHeadlines(newsApiService, query.query, query.country, lastRequestedPage, NETWORK_PAGE_SIZE, { articles, totalResults ->
            // label top headlines articles as such so we can query them later
            topHeadlinesTotalResults = totalResults
            for (article in articles) {
                article.type = TOP_HEADLINES_ARTICLE
            }
            doAsync {
                articleDao.insertArticles(articles).also {
                    lastRequestedPage++
                    isRequestInProgress = false
                }
            }
        }, { error ->
            Log.d(TAG, "Network error")
            networkErrors.postValue(error)
            isRequestInProgress = false
        })
    }

    /**
     * Public function to get more headlines from the api
     * If no query is passed in it will use the cache query
     */
    fun requestMoreTopHeadlines(query: Query? = null) {
        if (query == null) {
            queryAndInsertTopHeadlines(cachedQuery)
        } else {
            queryAndInsertTopHeadlines(query)
        }
    }

    /**
     * Start the search of top headlines
     */
    fun searchTopHeadlines(query: Query) : ArticleSearchResult {
        Log.i(TAG, "Search top headlines")

        // Purge old data so it doesn't get mixed into the new data
        doAsync {
            articleDao.deleteArticlesByType(TOP_HEADLINES_ARTICLE)
        }

        // reset page number
        lastRequestedPage = 1

        // get some new data
        cachedQuery = query
        requestMoreTopHeadlines(query)

        val dataSourceFactory = articleDao.getArticles(TOP_HEADLINES_ARTICLE)

        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(NewsListBoundaryCallback(this)).build()

        return ArticleSearchResult(data, networkErrors)
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 50
        private const val DATABASE_PAGE_SIZE = 20

        private const val TOP_HEADLINES_ARTICLE = "top_headlines"
        private const val EVERYTHING_ARTICLE = "everything"

        private val TAG = "News.NewsRepository"
        private var INSTANCE: NewsRepository? = null
        private val lock = Any()

        fun getInstance(articleDao: ArticleDao, newsApiService: NewsApiService): NewsRepository {
            if (INSTANCE == null) {
                synchronized(lock) {
                    INSTANCE = NewsRepository(articleDao, newsApiService)
                }
            }

            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}