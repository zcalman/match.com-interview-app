package com.zachcalman.matchinterviewapp.data

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

data class ArticleSearchResult (
    val data: LiveData<PagedList<Article>>,
    val networkErrors: LiveData<String>
)