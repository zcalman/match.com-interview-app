package com.zachcalman.matchinterviewapp.data.paging

import android.util.Log
import androidx.paging.PagedList
import com.zachcalman.matchinterviewapp.data.Article
import com.zachcalman.matchinterviewapp.data.repositories.NewsRepository

/**
 * Boundary callback that is attached to the PagedList generated in our news repository
 * This will take care of loading extra data when the list runs out
 */
class NewsListBoundaryCallback(
    private val newsRepository: NewsRepository
): PagedList.BoundaryCallback<Article>() {
    override fun onItemAtEndLoaded(itemAtEnd: Article) {
        super.onItemAtEndLoaded(itemAtEnd)
        Log.i(TAG, "End of list reached")
        newsRepository.requestMoreTopHeadlines()
    }

    companion object {
        const val TAG = "News.BoundaryCallback"
    }
}