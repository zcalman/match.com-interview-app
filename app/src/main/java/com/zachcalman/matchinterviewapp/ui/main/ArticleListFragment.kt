package com.zachcalman.matchinterviewapp.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.zachcalman.matchinterviewapp.MainActivity
import com.zachcalman.matchinterviewapp.R
import com.zachcalman.matchinterviewapp.ui.components.ArticleListAdapter
import kotlinx.android.synthetic.main.main_fragment.*

class ArticleListFragment : Fragment() {

    companion object {
        fun newInstance() = ArticleListFragment()
        const val TAG = "News.ListScreen"
    }

    private lateinit var viewModel: ArticleListViewModel
    private val adapter = ArticleListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = (activity as MainActivity).obtainArticleListViewModel()
        setRefreshState(true)
        viewModel.searchTopHeadlines(Query("trump", ""))
        initAdapter()
        initSwipeRefresh()
    }

    private fun initAdapter() {
        news_recycler_view.adapter = adapter
        viewModel.articles.observe(this@ArticleListFragment, Observer { list ->
            Log.d(TAG, "LiveData emitted")
            adapter.submitList(list)
            setRefreshState(false)
        })
    }

    private fun initSwipeRefresh() {
        recycler_refresh_container.setOnRefreshListener {
            viewModel.searchTopHeadlines(Query("gun", ""))
        }
    }

    private fun setRefreshState(state: Boolean) {
        recycler_refresh_container.isRefreshing = state
    }
}