package com.zachcalman.matchinterviewapp.ui.main

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.zachcalman.matchinterviewapp.data.Article
import com.zachcalman.matchinterviewapp.data.ArticleSearchResult
import com.zachcalman.matchinterviewapp.data.repositories.NewsRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync

class ArticleListViewModel (
    private val newsRepository: NewsRepository
) : ViewModel() {

    private val searchResult = MutableLiveData<ArticleSearchResult>()

    private lateinit var query: Query

    // These are our public LiveData for observing articles and network errors if they occur
    // They listen for when a new search result and split up the data
    val articles: LiveData<PagedList<Article>> = Transformations.switchMap(searchResult) { it.data }
    val networkErrors: LiveData<String> = Transformations.switchMap(searchResult) { it.networkErrors }

    fun searchTopHeadlines(query: Query) {
        viewModelScope.launch {
            val data = newsRepository.searchTopHeadlines(query)
            searchResult.value = data
        }
    }

    /**
     * Get the last query value.
     */
    fun lastQueryValue(): Query? = query

    companion object {
        private const val VISIBLE_THRESHOLD = 5
    }
}

data class Query(
    val query: String,
    val country: String
)