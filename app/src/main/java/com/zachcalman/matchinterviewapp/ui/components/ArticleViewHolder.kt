package com.zachcalman.matchinterviewapp.ui.components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zachcalman.matchinterviewapp.data.Article
import com.zachcalman.matchinterviewapp.databinding.ArticleListItemBinding
import com.zachcalman.matchinterviewapp.ui.main.ArticleListFragmentDirections
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * ViewHolder for an [Article] list item
 */
class ArticleViewHolder(
    private val binding: ArticleListItemBinding,
    private val animationDuration: Int
): RecyclerView.ViewHolder(binding.root) {

    private val prettyFormatter = DateTimeFormatter.ofPattern("EEEE',' MMMM d',' yyyy 'at' HH:mm")

    /**
     * Bind the article to the data binding layout Article variable and all the text will automatically be set
     */
    fun bind(article: Article) {
        binding.apply {
            binding.article = article

            // Don't show the article's author if there is none
            if (article.author == null) {
                author.visibility = View.GONE
            }

            // Load the image into the ImageView with Glide. That was easy!
            if (article.imageUrl != null) {
                Glide.with(itemView).load(article.imageUrl).into(articleImage)
            }

            val date = OffsetDateTime.parse(article.publishedAt)
            published.text = date.format(prettyFormatter)

            val expanded = article.expanded
            if (expanded) {
                fadeViewIn(collapsedContainer)
            } else {
                fadeViewOut(collapsedContainer)
            }

            openArticleButton.setOnClickListener { view ->
                val url = article.articleUrl
                val action = ArticleListFragmentDirections.actionMainFragmentToBrowserFragment().setArticleUrl(url!!)
                view.findNavController().navigate(action)
            }
        }
    }

    private fun fadeViewIn(view: View) {
        view.apply {
            alpha = 0f
            visibility = View.VISIBLE
            animate()
                .alpha(1f)
                .setDuration(animationDuration.toLong())
                .setListener(null)
        }
    }

    private fun fadeViewOut(view: View) {
        view.apply {
            alpha = 1f
            visibility = View.GONE
        }
    }

    companion object {
        fun create(parent: ViewGroup): ArticleViewHolder {
            return ArticleViewHolder(ArticleListItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false),
                parent.resources.getInteger(android.R.integer.config_shortAnimTime)
            )
        }
    }
}