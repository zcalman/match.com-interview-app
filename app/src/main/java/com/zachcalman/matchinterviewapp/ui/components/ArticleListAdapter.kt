package com.zachcalman.matchinterviewapp.ui.components

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.zachcalman.matchinterviewapp.data.Article

class ArticleListAdapter
    : PagedListAdapter<Article, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ArticleViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val article: Article? = getItem(position)
        if (article != null) {
            (holder as ArticleViewHolder).bind(article)

            holder.itemView.setOnClickListener { view ->
                val expanded = article.expanded
                article.expanded = !expanded
                notifyItemChanged(position)
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Article>() {
            override fun areItemsTheSame(oldArticle: Article, newArticle: Article): Boolean =
                oldArticle == newArticle

            override fun areContentsTheSame(oldArticle: Article, newArticle: Article): Boolean =
                oldArticle.uid == newArticle.uid
        }
    }
}