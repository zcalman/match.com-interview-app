package com.zachcalman.matchinterviewapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.browser_fragment.*

/**
 * Fragment responsible for displaying the full article in a webview
 */
class BrowserFragment : Fragment() {

    val args: BrowserFragmentArgs by navArgs()

    companion object {
        fun newInstance() = BrowserFragment()
    }

    private lateinit var viewModel: BrowserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.browser_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BrowserViewModel::class.java)
        // TODO: Use the ViewModel
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        article_webview.webViewClient = object : WebViewClient() {
//            override fun shouldOverrideUrlLoading(
//                view: WebView?,
//                request: WebResourceRequest?
//            ): Boolean {
//                return true
//            }
//        }

        article_webview.webViewClient = WebViewClient()
        val url = args.articleUrl
        val settings = article_webview.settings
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        article_webview.loadUrl(url)
    }
}
