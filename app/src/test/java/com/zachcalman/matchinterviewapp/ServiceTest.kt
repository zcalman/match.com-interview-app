package com.zachcalman.matchinterviewapp

import com.zachcalman.matchinterviewapp.data.remote.ApiFactory
import com.zachcalman.matchinterviewapp.data.remote.NewsApiService
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ServiceTest {
    private lateinit var service: NewsApiService

    @BeforeEach
    internal fun setup() {
        service = ApiFactory.newsApiService
    }

    @Test
    fun should_callTopHeadlines() {
        runBlocking {
            val articles = service.topHeadlines("us", null, null, null, null, null)
            println("top articles: ${articles.totalResults}")
            assertThat(articles.status, equalTo("ok"))
        }
    }

    @Test
    fun should_callEverything() {
        runBlocking {
            val articles = service.everything("android", null, null, null, null, null, null, null, null, null, null)
            println("everything articles: ${articles.totalResults}")
            assertThat(articles.status, equalTo("ok"))
        }
    }

    @Test
    fun should_callSources() {
        runBlocking {
            val sources = service.sources(null, "en", null)
            println("sources: ${sources.sources.size}")
            assertThat(sources.status, equalTo("ok"))
        }
    }
}