Welcome to my little news app!

This app displays a list of current top headlines. Tap on a headline to get some more information about it, and even read the full article!

Pull down to refresh top articles.

Search function has been implemented in the develop branch, go check it out! You can query both "everything" and "top headlines".